var app = new Vue({ 
    el: '#app',
    data: {
        message: 'Hello Vue!'
    }
});

//app.message = '改了一下';

var app1 = new Vue({
    el: '#app1',
    data: {
      seen: true
    }
  })
  
//app1.seen = false;

var app2 = new Vue({
    el: '#app2',
    data: {
      message: '哈囉~你好！'
    },
    methods: {
      reverseMessage: function () {
        this.message = this.message.split('').reverse().join('')
      }
    }
  })

  Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
  })
  
  var app3 = new Vue({
    el: '#app3',
    data: {
      groceryList: [
        { id: 0, text: '這是一' },
        { id: 1, text: '那這就是二' },
        { id: 2, text: '這難道會是三嗎' }
      ]
    }
  })