package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.bean.User;
import com.example.demo.dao.UserDao;

@RestController
public class UserController {
	@Autowired
	private UserDao userDao;

/**
* 根據郵件去查詢
* 
* @param email
* @return
*/
@RequestMapping(value = "/findUserByEmail")
public Object getUserByEmail(String email) {
System.out.println("email:" +  email);
User user = userDao.findByEmail(email);
if (null == user) {
return "暫無資料";
} else {
return user;
}
}

	/**
	 * 獲取所有的使用者資訊
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getall")
	public Object getAllUser() {
		List<User> list = (List<User>) userDao.findAll();
		if (null == list || list.size() == 0) {
			return "暫無資料";
		} else {
			return list;
		}
	}

/**
* 刪除指定id使用者
* 
* @param id
* @return
*/	
@RequestMapping(value = "/deleteUser")
public Object deleteuUser(int id) {
User user = userDao.findOne(id);
if (null == user) {
return "刪除使用者失敗:" +  id  + "沒找到該使用者";
} else {
userDao.delete(id);
return "刪除使用者成功:" +  id;
}
}

/**
* 新增使用者
* 
* @param id
* @param email
* @param name
* @return
*/
@RequestMapping(value = "/adduser")
public Object addUser(String id, String email, String name) {
System.out.println("email:" +  email);
int tempId = Integer.parseInt(id);
System.out.println("tempId:" +  tempId +  "email:" +  email +  "name:" +  name);
User tempUser = userDao.findOne(tempId);
if (null == tempUser) {
tempUser = new User();
tempUser.setId(tempId);
}
tempUser.setEmail(email);
tempUser.setName(name);
User resultUser = userDao.save(tempUser);
if (null == resultUser) {
return "新增使用者失敗";
} else {
return  "新增使用者:"  + resultUser.getName();
}
}

// 條件查詢
	/**
	 * 獲取姓名和郵箱是指定內容的使用者
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getUser1")
	public Object getUser(String email, String name) {
		List<User> userList = userDao.findByNameAndEmail(name, email);
		if (null != userList && userList.size() != 0) {
			return userList;
		} else {
			return "沒找到符合要求的使用者";
		}
	}

	/**
	 * 獲取姓名和郵箱是指定內容的使用者並排序
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getUser2")
	public Object getUser2(String email, String name) {
		List<User> userList = userDao.findByNameAndEmailOrderByIdDesc(name, email);
		if (null != userList && userList.size() != 0) {
			return userList;
		} else {
			return "沒找到符合要求的使用者";
		}
	}

	/**
	 * 獲取姓名和郵箱是指定內容的使用者並排序,前兩個
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getUser3")
	public Object getUser3(String email, String name) {
		List<User> userList = userDao.findTop2ByNameAndEmailOrderByIdDesc(name, email);
		if (null != userList && userList.size() != 0) {
			return userList;
		} else {
			return "沒找到符合要求的使用者";
		}
	}

	/**
	 * 分頁獲取郵箱為指定內容的資料
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getUser4")
	public Object getUser4(String email, int page) {
// page 屬於下標 從0開始 0代表是第一頁
		List<User> userList = userDao.findByEmail(email, new PageRequest(page, 2));
		if (null != userList && userList.size() != 0) {
			return userList;
		} else {
			return "沒找到符合要求的使用者";
		}
	}
}