package com.example.demo.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.example.demo.bean.User;
import java.lang.String;
import java.util.List;

@Transactional
@Repository
public interface UserDao extends CrudRepository<User, Integer> {
//jpa 方法名就是查詢語句,只要規法寫方法名一切就都可以完成(當然.有時候會造成方法名又臭又長)
User findByEmail(String email);//根據郵箱查詢
List<User> findByName(String name);//根據使用者名稱查詢
//select * from test.users where email='imgod@qq.com' and name='imgod';
List<User> findByNameAndEmail(String name,String email);//根據使用者名稱和郵箱進行查詢
//select * from test.users where email='imgod@qq.com' and name='imgod4444' order by id desc;
List<User> findByNameAndEmailOrderByIdDesc(String name,String email);//根據使用者名稱和郵箱進行查詢,排序
//select * from test.users where email='imgod@qq.com' and name='imgod4444' order by id desc limit 2;
List<User> findTop2ByNameAndEmailOrderByIdDesc(String name,String email);//根據使用者名稱和郵箱進行查詢,排序,前兩個
//根據郵箱進行分頁查詢
List<User> findByEmail(String email,Pageable pageable);//根據使用者名稱和郵箱進行查詢
}